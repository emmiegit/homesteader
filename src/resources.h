/*
 * resources.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RESOURCES_H
#define __RESOURCES_H

#include "game.h"
#include "level.h"

struct resource {
	enum {
		RESOURCE_TYPE_GAME,
		RESOURCE_TYPE_LEVEL
	} t;

	union {
		struct game game;
		struct level level;
	} u;
};

int resources_setup(void);
void resources_cleanup(void);

int resource_load(struct resource *res, const char *filename);
int resource_save(const struct resource *res, const char *filename);

#endif /* __RESOURCES_H */

/* vim: set ft=c: */
