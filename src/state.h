/*
 * state.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __STATE_H
#define __STATE_H

#include "level.h"
#include "menu.h"

/* Lock this structure before changing it */

struct state {
	enum {
		STATE_TYPE_LEVEL,
		STATE_TYPE_MENU
	} type;
	union {
		struct level *lvl;
		struct menu menu;
	} u;
};

void state_setup(void);
void state_cleanup(void);

const struct state *state_r_get(void);
struct state *state_w_get(void);
void state_release(const struct state *st);

#endif /* __STATE_H */

/* vim: set ft=c: */
