/*
 * resources.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <locale.h>

#include "arguments.h"
#include "core.h"
#include "resources.h"
#include "serial.h"

int resources_setup(void)
{
	setlocale(LC_ALL, "");
	return 0;
}

void resources_cleanup(void)
{
	/* nothing to do */
}

int resource_load(struct resource *res, const char *filename)
{
	switch (res->t) {
	case RESOURCE_TYPE_GAME:
		return read_game(filename, &res->u.game);
	case RESOURCE_TYPE_LEVEL:
		return read_level(filename, &res->u.level);
	default:
		die("Invalid resource type: %u.", res->t);
	}
}

int resource_save(const struct resource *res, const char *filename)
{
	switch (res->t) {
	case RESOURCE_TYPE_GAME:
		return write_game(filename, &res->u.game);
	case RESOURCE_TYPE_LEVEL:
		return write_level(filename, &res->u.level);
	default:
		die("Invalid resource type: %u.", res->t);
	}
}
