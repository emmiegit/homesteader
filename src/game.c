/*
 * game.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entity.h"
#include "game.h"

struct game game;

void game_setup(void)
{
	game.tick = 0;
	game.state = GAME_STOPPED;
	game.player.x = 0;
	game.player.y = 0;
}

void *game_mainloop(void *arg)
{
	UNUSED(arg);

	journal("Beginning game main loop.");
	for (;;) {
		sleep(0);
	}
}

void game_cleanup(void)
{
	/* nothing to do */
}
