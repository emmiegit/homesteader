/*
 * sound.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ao/ao.h>

#include "core.h"
#include "sound.h"

struct ao_sample_format format;
ao_device *dev;
int driver;

void sound_setup(void)
{
	ao_initialize();

#if 0
	driver = ao_default_driver_id();
#else
	driver = ao_driver_id("null");
#endif

	/* TODO replace with actual formatting */
	format.bits = 16;
	format.channels = 2;
	format.rate = 44100;
	format.byte_format = AO_FMT_LITTLE;

	dev = ao_open_live(driver, &format, NULL);
	if (!dev) {
		pdie("Error opening audio device.");
	}
}

void sound_cleanup(void)
{
	if (dev) {
		ao_close(dev);
	}
	ao_shutdown();
}

int sound_play(const char *name)
{
	char buf[4096];

	/* TODO */
	UNUSED(name);

	if (!ao_play(dev, buf, sizeof(buf))) {
		journal("Error writing to audio device.");
		ao_close(dev);
		dev = NULL;
		return -1;
	}
	return 0;
}

int sound_works(void)
{
	return dev != NULL;
}

void sound_volume(int delta)
{
	/* TODO */
	UNUSED(delta);
}
