/*
 * journal.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <time.h>

#include "arguments.h"
#include "journal.h"

static FILE *journal_fh = NULL;

int journal_setup(void)
{
	if (!opt.journal_path) {
		return 0;
	}
	journal_fh = fopen(opt.journal_path, "w");
	if (!journal_fh) {
		return -1;
	}
	journal("Opened journal.");
	return 0;
}

void journal_close(void)
{
	if (!journal_fh) {
		return;
	}
	journal("Closing journal.");
	if (fclose(journal_fh)) {
		TRACE();
	}
	journal_fh = NULL;
}

void journal(const char *format, ...)
{
	static char time_buf[23];
	static time_t last_time;
	time_t cur_time;
	va_list args;

	if (!journal_fh) {
		return;
	}
	cur_time = time(NULL);
	if (cur_time != last_time) {
		strftime(time_buf, sizeof(time_buf), "%Y-%m-%d %I:%M:%S %p", localtime(&cur_time));
		last_time = cur_time;
	}
	fprintf(journal_fh, "[%s] ", time_buf);
	va_start(args, format);
	vfprintf(journal_fh, format, args);
	fputc('\n', journal_fh);
	va_end(args);
}
