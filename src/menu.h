/*
 * menu.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MENU_H
#define __MENU_H

struct menu {
	enum menu_stage {
		MENU_STAGE_MAIN,
		MENU_STAGE_NEW,
		MENU_STAGE_LOAD,
		MENU_STAGE_OPTIONS,
		NUM_MENU_STAGES
	} stage;

	union {
		struct {
			enum main_menu_item {
				MENU_ITEM_NEW,
				MENU_ITEM_LOAD,
				MENU_ITEM_OPTIONS,
				MENU_ITEM_QUIT,
				NUM_MENU_ITEMS
			} sel;
		} main;

		struct {
			char _placeholder;
		} new;

		struct {
			char _placeholder;
		} load;

		struct {
			char _placeholder;
		} options;
	} u;
};

void menu_select(void);

#endif /* __MENU_H */

/* vim: set ft=c: */
