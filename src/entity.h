/*
 * entity.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ENTITY_H
#define __ENTITY_H

#include <stddef.h>

#include "colors.h"
#include "position.h"

struct entity {
	struct position pos;
	struct {
		const char *str;
		enum color color;
		int attr;
	} sprite;
};

struct entity_list {
	size_t size;
	struct entity *list;
};

void entity_create(struct entity *ent, const char *str, unsigned int x, unsigned int y);

#endif /* __ENTITY_H */

/* vim: set ft=c: */
