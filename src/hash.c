/*
 * hash.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hash.h"

#define HASH_START	5381

unsigned long djb2_hash(const char *buf, size_t len)
{
	unsigned long hash = HASH_START;
	size_t i;

	for (i = 0; i < len; i++) {
		hash = ((hash << 5) + hash) + buf[i];
	}
	return hash;
}

unsigned long djb2_str_hash(const char *str)
{
	unsigned long hash = HASH_START;
	size_t i;

	for (i = 0; str[i]; i++) {
		hash = ((hash << 5) + hash) + str[i];
	}
	return hash;
}
