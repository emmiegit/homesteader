/*
 * colors.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curses.h>

#include "colors.h"
#include "core.h"

void colors_init(void)
{
	if (!has_colors()) {
		journal("No color support on this terminal.");
		return;
	}
	if (start_color()) {
		die("Cannot start color mode.");
	}
	if (use_default_colors()) {
		journal("Can't use default terminal colors.");
	}

	/* init_pair(color_name, foreground, background) */
	CURSES_CALL(init_pair(CLR_VISIT, -1, COLOR_BLACK));
	CURSES_CALL(init_pair(CLR_SELECT, COLOR_CYAN, -1));
	CURSES_CALL(init_pair(CLR_ADD, COLOR_CYAN, -1));
	CURSES_CALL(init_pair(CLR_REMOVE, COLOR_RED, -1));
	CURSES_CALL(init_pair(CLR_MOVE, COLOR_YELLOW, -1));
}
