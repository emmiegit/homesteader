/*
 * bitset.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "bitset.h"
#include "core.h"

#define BITS_PER_BLOCK		(sizeof(uintmax_t) * 8)
#define BYTES_PER_BITSET(bs)	(sizeof(uintmax_t) * (bs)->block_count)

int bitset_create(struct bitset *bs, unsigned int size, int initial)
{
	char blk;

	bs->size = size;
	bs->block_count = ceildiv(size, BITS_PER_BLOCK);
	bs->blocks = malloc(BYTES_PER_BITSET(bs));
	if (!bs->blocks) {
		return -1;
	}
	blk = initial ? 0xff : 0x00;
	memset(bs->blocks, blk, BYTES_PER_BITSET(bs));
	return 0;
}

void bitset_destroy(struct bitset *bs)
{
	free(bs->blocks);
}

int bitset_get(const struct bitset *bs, unsigned int index)
{
	size_t blockno, bitoff;

	ASSERT(index < bs->size);

	blockno = index / BITS_PER_BLOCK;
	bitoff  = index % BITS_PER_BLOCK;
	return (bs->blocks[blockno] >> bitoff) & 1;
}

void bitset_set(struct bitset *bs, unsigned int index)
{
	size_t blockno, bitoff;

	ASSERT(index < bs->size);

	blockno = index / BITS_PER_BLOCK;
	bitoff  = index % BITS_PER_BLOCK;
	bs->blocks[blockno] |= (1 << bitoff);
}

void bitset_clear(struct bitset *bs, unsigned int index)
{
	size_t blockno, bitoff;

	ASSERT(index < bs->size);

	blockno = index / BITS_PER_BLOCK;
	bitoff  = index % BITS_PER_BLOCK;
	bs->blocks[blockno] &= ~(1 << bitoff);
}

void bitset_toggle(struct bitset *bs, unsigned int index)
{
	size_t blockno, bitoff;

	ASSERT(index < bs->size);

	blockno = index / BITS_PER_BLOCK;
	bitoff  = index % BITS_PER_BLOCK;
	bs->blocks[blockno] ^= (1 << bitoff);
}

void bitset_assign(struct bitset *bs, unsigned int index, int value)
{
	uintmax_t blk;
	size_t blockno, bitoff;

	ASSERT(index < bs->size);

	blockno = index / BITS_PER_BLOCK;
	bitoff  = index % BITS_PER_BLOCK;
	blk = bs->blocks[blockno];
	value = !!value;

	bs->blocks[blockno] ^= (-value ^ blk) & (1 << bitoff);
}
