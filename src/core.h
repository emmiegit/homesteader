/*
 * core.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __CORE_H
#define __CORE_H

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Definitions */

#define PROGRAM_NAME				"homesteader"
#define PROGRAM_VERSION_MAJOR			0
#define PROGRAM_VERSION_MINOR			0
#define PROGRAM_VERSION_PATCH			7

#ifndef GIT_HASH
# define GIT_HASH				"nogithash"
#endif /* GIT_HASH */

#if defined(__clang__)
# define COMPILER_NAME				"Clang/LLVM"
# define COMPILER_VERSION			__clang_version__
#elif defined(__ICC) || defined(__INTEL_COMPILER)
# define COMPILER_NAME				"Intel ICC"
# define COMPILER_VERSION			__INTEL_COMPILER
#elif defined(__MINGW32__)
# define COMPILER_NAME				"Mingw"
# define COMPILER_VERSION			__VERSION__
#elif defined(__GNUC__) || defined(__GNUG__)
# define COMPILER_NAME				"GCC"
# define COMPILER_VERSION			__VERSION__
#elif defined(__HP_cc) || defined(__HP_aCC)
# define COMPILER_NAME				"Hewlett-Packard C"
# define COMPILER_VERSION			__HP_cc
#elif defined(__IBMC__) || defined(__IBMCPP__)
# define COMPILER_NAME				"IBM XL C"
# define COMPILER_VERSION			__xlc__
#elif defined(_MSC_VER)
# define COMPILER_NAME				"Microsoft Visual Studio"
# define COMPILER_VERSION			_MSC_VER
#elif defined(__PGI)
# define COMPILER_NAME				"Portland Group PGCC"
# define COMPILER_VERSION			__PGIC__
#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
# define COMPILER_NAME				"Oracle Solaris Studio"
# define COMPILER_VERSION			__SUNPRO_C
#else
# define COMPILER_NAME				"Unknown"
# define COMPILER_VERSION			"compiler"
#endif

#if defined(_WIN64)
# define PLATFORM_NAME				"Windows 64-bit"
#elif defined(_WIN32)
# define PLATFORM_NAME				"Windows 32-bit"
#elif defined(__linux__)
# define PLATFORM_NAME				"Linux"
#elif defined(__APPLE__) || defined(__MACH__)
# define PLATFORM_NAME				"Apple"
#elif defined(__FreeBSD__)
# define PLATFORM_NAME				"FreeBSD"
#elif defined(__unix) || defined(__unix__)
# define PLATFORM_NAME				"Unknown UNIX"
#else
# define PLATFORM_NAME				"Unknown"
#endif

/* Macros and inline functions */

#define UNUSED(x)				((void)(x))
#define DEFAULT(x,alt)				((x) ? (x) : (alt))
#define STATIC_ASSERT(x)			((void)sizeof(char[2 * (!!(x)) - 1]))
#define ARRAY_SIZE(x)				(sizeof(x) / (sizeof((x)[0])))
#define GET_VOLATILE(x,t)			(*(volatile t *)(&(x)))

#if defined(NDEBUG)
# define ASSERT(x)				((void)0)
# define FINAL_FREE(x)				((void)(x))
# define CURSES_CALL(x)				((void)(x))
#else
# define ASSERT(x)				\
	do {					\
		if (x) {			\
			break;			\
		}				\
		TRACE();			\
		die("Assertion failed!\n"	\
		    "%s:%d: ASSERT(%s)\n",	\
		    __FILE__, __LINE__,  #x);	\
	} while (0)
# define FINAL_FREE(x)				\
	do {					\
		free(x);			\
	} while (0)
# define CURSES_CALL(x)				\
	do {					\
		if ((x) != ERR) {		\
			break;			\
		}				\
		TRACE();			\
		die("Curses call failed!\n"	\
		    "%s:%d: %s\n",		\
		    __FILE__, __LINE__, #x);	\
	} while (0)
#endif /* NDEBUG */

#define SWAP(a,b,t)				\
	do {					\
		t __temp_var_ = (a);		\
		(a) = (b);			\
		(b) = __temp_var_;		\
	} while (0)

#define TRACE()								\
	do {								\
		int __trc_e = errno;					\
		if (errno) {						\
			journal("Trace: %s:%d returned %d: %s",		\
				__FILE__, __LINE__,			\
				errno, strerror(errno));		\
		} else {						\
			journal("Trace: %s:%d.", __FILE__, __LINE__);	\
		}							\
		errno = __trc_e;					\
	} while (0)

/* GNU extensions */

#if defined(__GNUC__)
# define NORETURN	__attribute__((noreturn))
# define INLINE		static __attribute__((unused))
# define IS_PRINTF	__attribute__((format(printf, 1, 2)))

# define likely(x)	(__builtin_expect(!!(x), 1)
# define unlikely(x)	(__builtin_expect(!!(x), 0)

INLINE void hint_pause(void)
{
	__asm__("pause");
}

#else
# define NORETURN
# define INLINE		static
# define IS_PRINTF

# define likely(x)	(x)
# define unlikely(x)	(x)

INLINE void hint_pause(void)
{
	(void)sleep(0);
}

#endif /* __GNUC__ */

INLINE int min(int a, int b)
{
	return ((a < b) ? a : b);
}

INLINE int max(int a, int b)
{
	return ((a > b) ? a : b);
}

INLINE int floordiv(int a, int b)
{
	return ((a / b) * b);
}

INLINE int ceildiv(int a, int b)
{
	return ((a % b == 0) ? (a / b) : ((a / b) + 1));
}

INLINE int bound(int x, int min, int max)
{
	return ((min >= x) ? min : ((x >= max) ? max : x));
}

/* External symbol declarations */

void core_setup(void);
void print_version(void);

#define proper8(x)	(x)
uint16_t proper16(uint16_t x);
uint32_t proper32(uint32_t x);
uint64_t proper64(uint64_t x);

NORETURN void die(const char *format, ...) IS_PRINTF;
NORETURN void pdie(const char *message);

#include "journal.h"

#endif /* __CORE_H */

/* vim: set ft=c: */
