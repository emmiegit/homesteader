/*
 * serial.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdint.h>

#include "game.h"
#include "level.h"

/*
 * We use the fixed-width integer types to ensure save files will be read
 * the same way across different platforms. We use #pragma pack to ensure
 * that no padding is injected in the file formatting, as this too differs
 * by platform.
 *
 * In our struct definitions, we use "header" to denote fixed-size data
 * items and "var" to denote variable-size data items.
 */

enum serial_error {
	SERIAL_OK,
	SERIAL_ERR_OPEN,
	SERIAL_ERR_READ,
	SERIAL_ERR_WRITE,
	SERIAL_ERR_BADMAGIC,
	SERIAL_ERR_BADVER,
	SERIAL_ERR_INVALID,
	SERIAL_ERR_ALLOC,
	SERIAL_ERR_IO
};

#define SAVE_MAGIC			"\xbbMiv"
#define SAVE_MAGIC_LENGTH		4
#define SAVE_VERSION			0

#pragma pack(push, 1)
struct save_file_header {
	uint8_t magic[SAVE_MAGIC_LENGTH];	/* t == SAVE_MAGIC */
	uint32_t version;			/* t == SAVE_VERISION */

	int64_t start_time;
	uint32_t elapsed_time;

	uint32_t keys_pressed;
	uint32_t actions_execd;
	uint32_t chars_inserted;
	uint32_t chars_deleted;
	uint32_t chars_yanked;
	uint32_t chars_put;
	uint16_t levels_played;
	uint16_t bosses_defeatd;

	/* TODO level progress */
};
#pragma pack(pop)

#define LEVEL_MAGIC			"\xbbMIV"
#define LEVEL_MAGIC_LENGTH		4
#define LEVEL_VERSION			0

#pragma pack(push, 1)
struct level_file_header {
	uint8_t magic[LEVEL_MAGIC_LENGTH];	/* t == LEVEL_MAGIC */
	uint32_t version;			/* t == LEVEL_VERSION */

	uint16_t width;				/* t > MIN_LEVEL_WIDTH */
	uint16_t height;			/* t > MIN_LEVEL_HEIGHT */
	uint8_t start_pos;			/* t in "enum level_pos" */
	uint16_t end_pos_x;			/* t < width */
	uint16_t end_pos_y;			/* t < height */

	uint16_t max_time;
	uint16_t max_key_presses;

	uint8_t add_count;
	uint8_t delete_count;
	uint8_t change_count;
	uint8_t move_count;
};

struct level_file_var {
	struct level_add_entities {		/* len == add_count */
		uint16_t end_pos_x;		/* t < width */
		uint16_t end_pos_y;		/* t < height */
		uint8_t string_len;		/* t != 0 */

		/* ... */

		const char *string;		/* len == string_len */
	} *add_entities;

	struct level_delete_entities {
		uint16_t start_pos_x;		/* t < width */
		uint16_t start_pos_y;		/* t < height */
		uint8_t string_len;		/* t != 0 */

		/* ... */

		const char *string;		/* len == string_len */
	} *delete_entities;

	struct level_change_entities {
		uint16_t start_pos_x;		/* t < width */
		uint16_t start_pos_y;		/* t < height */
		uint8_t string_len;		/* t != 0 */

		/* ... */

		const char *string;
	} *change_entities;

	struct level_move_entities {
		uint16_t start_pos_x;		/* t < width */
		uint16_t start_pos_y;		/* t < height */
		uint16_t end_pos_x;		/* t < width */
		uint16_t end_pos_y;		/* t < height */
		uint8_t string_len;		/* t != 0 */

		/* ... */

		const char *string;		/* len == string_len */
	} *move_entities;

	uint8_t *map_tiles;			/* len == (width * height) */
};
#pragma pack(pop)

void serial_setup(void);
void serial_cleanup(void);

enum serial_error write_game(const char *filename, const struct game *obj);
enum serial_error read_game(const char *filename, struct game *obj);
enum serial_error write_level(const char *filename, const struct level *obj);
enum serial_error read_level(const char *filename, struct level *obj);

#endif /* __SERIAL_H */

/* vim: set ft=c: */
