/*
 * map.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MAP_H
#define __MAP_H

#include <stdint.h>

#include "render/screen.h"

#include "entity.h"

typedef int_fast8_t map_tile;
enum map_tile {
	TILE_VOID,
	TILE_DIRT,
	TILE_ROCK,
	TILE_LAVA,
	NUM_TILES
};

struct map {
	unsigned int width;
	unsigned int height;

	map_tile *tiles;
	struct entity_list add_ents;
	struct entity_list delete_ents;
	struct entity_list change_ents;
	struct entity_list move_ents;
};

int map_create(struct map *map, int width, int height);
void map_destroy(struct map *map);

#endif /* __MAP_H */

/* vim: set ft=c: */
