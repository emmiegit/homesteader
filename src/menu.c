/*
 * menu.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core.h"
#include "main.h"
#include "menu.h"
#include "state.h"

void menu_select(void)
{
	struct state *st;
	struct menu *menu;

	st = state_w_get();
	menu = &st->u.menu;
	switch (menu->u.main.sel) {
	case MENU_ITEM_NEW:
		journal("Selected \"new game\".");
		menu->stage = MENU_STAGE_NEW;
		break;
	case MENU_ITEM_LOAD:
		journal("Selected \"load game\".");
		menu->stage = MENU_STAGE_LOAD;
		break;
	case MENU_ITEM_OPTIONS:
		journal("Selected \"options\".");
		menu->stage = MENU_STAGE_OPTIONS;
		break;
	case MENU_ITEM_QUIT:
		journal("Selected \"quit\"");
		cleanup(0);
		break;
	default:
		die("Invalid main menu item value: %u\n", menu->u.main.sel);
	}
	state_release(st);
}
