/*
 * game.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GAME_H
#define __GAME_H

#include <stdint.h>
#include <time.h>

#include "core.h"
#include "entity.h"

struct game {
	unsigned long tick;
	enum {
		GAME_RUNNING,
		GAME_PAUSED,
		GAME_STOPPED
	} state;

	struct position player;
	time_t start_time;
	time_t old_elapsed_time;

	struct {
		unsigned int keys_pressed;
		unsigned int actions_execd;
		unsigned int chars_inserted;
		unsigned int chars_deleted;
		unsigned int chars_yanked;
		unsigned int chars_put;
		unsigned short levels_played;
		unsigned short bosses_defeatd;
	} stats;
};

extern struct game game;

void game_setup(void);
NORETURN void *game_mainloop(void *arg);
void game_cleanup(void);

#endif /* __GAME_H */

/* vim: set ft=c: */
