/*
 * level.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core.h"
#include "level.h"

void level_init(struct level *lvl, struct map *map)
{
	lvl->map = map;
	lvl->mode = MODE_NORMAL;
	lvl->keys.count = 0;
}

int level_hist_append(struct level *lvl, char ch)
{
	if (lvl->keys.count < ARRAY_SIZE(lvl->keys.hist)) {
		lvl->keys.hist[lvl->keys.count++] = ch;
		return 0;
	}
	return -1;
}
