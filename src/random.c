/*
 * random.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include "random.h"

void random_setup(void)
{
	srand(time(NULL));
}

/* Range: [min, max) */
int random_int(int min, int max)
{
	return (rand() % max) + min;
}

/* Range: [0, 1) */
double random_float(void)
{
	return (double)rand() / RAND_MAX;
}

/* Mean: 0, Standard deviation: 1 */
double random_gauss(void)
{
	static double v1, v2, s;
	static int phase = 0;
	double x;

	if (!phase) {
		do {
			v1 = (double)rand() / RAND_MAX;
			v2 = (double)rand() / RAND_MAX;
			v1 = 2 * v1 - 1;
			v2 = 2 * v2 - 1;
			s = v1 * v1 + v2 * v2;
		} while (s >= 1 || s == 0);
		x = v1 * sqrt(-2 * log(s) / s);
	} else {
		x = v2 * sqrt(-2 * log(s) / s);
	}
	phase = !phase;
	return x;
}
