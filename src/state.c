/*
 * state.c
 * * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>

#include "core.h"
#include "state.h"

static pthread_rwlock_t state_lock = PTHREAD_RWLOCK_INITIALIZER;
static struct state state;

/* Utilities */

static void r_lock(void)
{
	if (pthread_rwlock_rdlock(&state_lock)) {
		TRACE();
	}
}

static void w_lock(void)
{
	if (pthread_rwlock_wrlock(&state_lock)) {
		TRACE();
	}
}

static void unlock(void)
{
	if (pthread_rwlock_unlock(&state_lock)) {
		TRACE();
	}
}

/* Externals */

const struct state *state_r_get(void)
{
	r_lock();
	return &state;
}

struct state *state_w_get(void)
{
	w_lock();
	return &state;
}

void state_release(const struct state *st)
{
	UNUSED(st);
	ASSERT(st == &state);
	unlock();
}

void state_setup(void)
{
	state.type = STATE_TYPE_MENU;
	state.u.menu.stage = MENU_STAGE_MAIN;
	state.u.menu.u.main.sel = MENU_ITEM_NEW;
}

void state_cleanup(void)
{
	if (pthread_rwlock_destroy(&state_lock)) {
		TRACE();
	}
}

