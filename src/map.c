/*
 * map.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "core.h"
#include "map.h"

int map_create(struct map *map, int width, int height)
{
	ASSERT(width > 0);
	ASSERT(height > 0);

	map->tiles = calloc(width * height, sizeof(map_tile));
	if (!map->tiles) {
		return -1;
	}
	map->width = width;
	map->height = height;
	return 0;
}

void map_destroy(struct map *map)
{
	FINAL_FREE(map->tiles);
}
