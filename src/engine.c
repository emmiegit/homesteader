/*
 * engine.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <pthread.h>

#include "render/screen.h"

#include "engine.h"
#include "game.h"
#include "nanotime.h"

/* To prevent the journal from getting too big */
#define JOURNAL_EACH_TICK	0

static struct {
	pthread_t game_thrd;
	pthread_t tick_thrd;
	pthread_t screen_thrd;
} engine;

void engine_start(void)
{
	game.state = GAME_RUNNING;
	if (pthread_create(&engine.screen_thrd,
			   NULL,
			   screen_mainloop,
			   NULL)) {
		pdie("Unable to start screen thread");
	}
	/*
	if (pthread_create(&engine.tick_thrd,
			   NULL,
			   engine_mainloop,
			   NULL)) {
		pdie("Unable to start tick thread");
	}
	if (pthread_create(&engine.game_thrd,
			   NULL,
			   game_mainloop,
			   NULL)) {
		pdie("Unable to start game thread");
	}
	*/
	curses_on();
	full_redraw();
}

void *engine_mainloop(void *arg)
{
	struct timespec now, last_tick;
	long elapsed;

	UNUSED(arg);

	journal("Beginning engine main loop.");
	get_nanotime(&last_tick);
	while (game.state != GAME_STOPPED) {
		if (game.state == GAME_PAUSED) {
			/* TODO */
			continue;
		}

		redraw();

		/* End of tick */
		get_nanotime(&now);
		elapsed = NANO_ELAPSED(last_tick, now);
#if JOURNAL_EACH_TICK
		journal("Tick %lu.", game.tick);
		journal("Current time is %ld seconds and %ld nanoseconds.", now.tv_sec, now.tv_nsec);
		journal("%ld nanoseconds have elapsed since the last tick.", elapsed);
#endif
		last_tick = now;
		if (elapsed < ENGINE_NANOS_PER_TICK) {
			struct timespec sleep_len;
			sleep_len.tv_sec = 0;
			sleep_len.tv_nsec = ENGINE_NANOS_PER_TICK - elapsed;
#if JOURNAL_EACH_TICK
			journal("Sleeping for %ld nanoseconds.", sleep_len.tv_nsec);
#endif
			nanosleep(&sleep_len, NULL);
		}
		game.tick++;
	}
	return NULL;
}

void engine_stop(void)
{
	curses_off();
	game.state = GAME_STOPPED;
	screen_event_hint();
	if (pthread_join(engine.screen_thrd, NULL)) {
		TRACE();
	}
	/*
	if (pthread_join(engine.tick_thrd, NULL)) {
		TRACE();
	}
	if (pthread_join(engine.game_thrd, NULL)) {
		TRACE();
	}
	*/
}
