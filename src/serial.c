/*
 * serial.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <unistd.h>

#include <stdlib.h>

#include "core.h"
#include "engine.h"
#include "map.h"
#include "serial.h"

#define WRITE_FLAGS			(O_CREAT | O_TRUNC | O_WRONLY)
#define READ_FLAGS			(O_RDONLY)

#define FILE_CLOSE(x)			\
	do {				\
		if (close(x)) {		\
			TRACE();	\
		}			\
	} while (0)

static size_t bufsize_level(const struct level *obj)
{
	size_t size;

	size = sizeof(struct level_file_header);
	size += sizeof(struct level_add_entities) * obj->map->add_ents.size;
	size += sizeof(struct level_delete_entities) * obj->map->delete_ents.size;
	size += sizeof(struct level_change_entities) * obj->map->change_ents.size;
	size += sizeof(struct level_move_entities) * obj->map->move_ents.size;
	/* TODO factor in string size :( */
	return size;
}

void serial_setup(void)
{
#pragma pack(push, 1)
	struct pragma_pack_test {
		uint8_t x;
		uint16_t y;
	};
#pragma pack(pop)

	STATIC_ASSERT(sizeof(struct pragma_pack_test) == 3);
}

void serial_cleanup(void)
{
	/* nothing to do */
}

enum serial_error write_game(const char *filename, const struct game *obj)
{
	struct save_file_header hdr;
	int fd;

	fd = open(filename, WRITE_FLAGS, 0777);
	if (fd < 0) {
		TRACE();
		return SERIAL_ERR_OPEN;
	}

	/* Copy constants */
	memcpy(hdr.magic, SAVE_MAGIC, SAVE_MAGIC_LENGTH);
	hdr.version = proper32(SAVE_VERSION);

	/* Copy fields */
	hdr.start_time = proper64(obj->start_time);
	hdr.elapsed_time = proper64(obj->old_elapsed_time + obj->tick * TICKS_PER_SECOND);
	hdr.keys_pressed = proper32(obj->stats.keys_pressed);
	hdr.actions_execd = proper32(obj->stats.actions_execd);
	hdr.chars_inserted = proper32(obj->stats.chars_inserted);
	hdr.chars_deleted = proper32(obj->stats.chars_deleted);
	hdr.chars_yanked = proper32(obj->stats.chars_yanked);
	hdr.chars_put = proper32(obj->stats.chars_put);
	hdr.levels_played = proper16(obj->stats.levels_played);
	hdr.bosses_defeatd = proper16(obj->stats.bosses_defeatd);

	/* Write and close */
	if (write(fd, &hdr, sizeof(hdr)) != sizeof(hdr)) {
		FILE_CLOSE(fd);
		return SERIAL_ERR_WRITE;
	}
	if (close(fd)) {
		TRACE();
		return SERIAL_ERR_IO;
	}
	return SERIAL_OK;
}

enum serial_error read_game(const char *filename, struct game *obj)
{
	struct save_file_header hdr;
	int fd;

	fd = open(filename, READ_FLAGS);
	if (fd < 0) {
		TRACE();
		return SERIAL_ERR_OPEN;
	}
	if (read(fd, &hdr, sizeof(hdr)) != sizeof(hdr)) {
		FILE_CLOSE(fd);
		return SERIAL_ERR_READ;
	}

	/* Check constants */
	if (memcmp(hdr.magic, SAVE_MAGIC, SAVE_MAGIC_LENGTH)) {
		return SERIAL_ERR_BADMAGIC;
	}
	if (hdr.version != proper32(SAVE_VERSION)) {
		return SERIAL_ERR_BADVER;
	}

	/* Copy fields */
	obj->start_time = proper64(hdr.start_time);
	obj->old_elapsed_time = proper64(hdr.elapsed_time);
	obj->stats.keys_pressed = proper64(hdr.keys_pressed);
	obj->stats.actions_execd = proper32(hdr.actions_execd);
	obj->stats.chars_inserted = proper32(hdr.chars_inserted);
	obj->stats.chars_deleted = proper32(hdr.chars_deleted);
	obj->stats.chars_yanked = proper32(hdr.chars_yanked);
	obj->stats.chars_put = proper32(hdr.chars_put);
	obj->stats.levels_played = proper16(hdr.levels_played);
	obj->stats.bosses_defeatd = proper16(hdr.bosses_defeatd);

	/* Close */
	if (close(fd)) {
		TRACE();
		return SERIAL_ERR_IO;
	}
	return SERIAL_OK;
}

enum serial_error write_level(const char *filename, const struct level *obj)
{
	char *buf, *ptr;
	struct level_file_header *hdr;
	int fd;

	/* Open file */
	fd = open(filename, WRITE_FLAGS, 0777);
	if (fd < 0) {
		TRACE();
		return SERIAL_ERR_OPEN;
	}

	/* Allocate complete buffer */
	buf = malloc(bufsize_level(obj));
	if (!buf) {
		int errsave = errno;
		TRACE();
		FILE_CLOSE(fd);
		errno = errsave;
		return SERIAL_ERR_ALLOC;
	}
	hdr = (void *)buf;
	ptr = buf + sizeof(struct level_file_header);

	/* Copy constants */
	memcpy(hdr->magic, LEVEL_MAGIC, LEVEL_MAGIC_LENGTH);
	hdr->version = proper32(LEVEL_VERSION);

	/* Copy fields */
	hdr->width = proper16(obj->map->width);
	hdr->height = proper16(obj->map->height);
	hdr->start_pos = proper8(obj->start);
	hdr->end_pos_x = proper16(obj->end.x);
	hdr->end_pos_y = proper16(obj->end.y);
	hdr->max_time = proper16(obj->max_time);
	hdr->max_key_presses = proper16(obj->max_key_presses);
	hdr->add_count = proper8(obj->map->add_ents.size);
	hdr->delete_count = proper8(obj->map->delete_ents.size);
	hdr->change_count = proper8(obj->map->change_ents.size);
	hdr->move_count = proper8(obj->map->move_ents.size);

	/* TODO loop through variable data */
	ptr += 0;

	/* Write and close */
	if (write(fd, &hdr, sizeof(hdr)) != sizeof(hdr)) {
		int errsave = errno;
		FILE_CLOSE(fd);
		free(buf);
		errno = errsave;
		return SERIAL_ERR_WRITE;
	}
	if (close(fd)) {
		int errsave = errno;
		TRACE();
		free(buf);
		errno = errsave;
		return SERIAL_ERR_IO;
	}
	free(buf);
	return SERIAL_OK;
}

enum serial_error read_level(const char *filename, struct level *lvl)
{
	/* TODO - copy from write_level() */

	UNUSED(filename);
	UNUSED(lvl);

	return -1;
}
