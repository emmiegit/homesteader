/*
 * nanotime.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __NANOTIME_H
#define __NANOTIME_H

#include <time.h>

int nanotime_setup(void);
void get_nanotime(struct timespec *ts);

#define NANOS_PER_SECOND	1000000000L
#define NANO_ELAPSED(a,b)	\
	((((b).tv_sec - (a).tv_sec) * NANOS_PER_SECOND) + (((b).tv_nsec - (a).tv_nsec)))

#endif /* __NANOTIME_H */

/* vim: set ft=c: */
