/*
 * nanotime.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "core.h"
#include "nanotime.h"

#if defined(__MACH__) || defined(__APPLE__)
# define HAS_MACH
# include <mach/clock.h>
# include <mach/mach.h>

int nanotime_setup(void)
{
	return 0;
}

void get_nanotime(struct timespec *ts)
{
	clock_serv_t cclock;
	mach_timespec_t mts;

	host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
	clock_get_time(cclock, &mts);
	mach_port_deallocate(mach_task_self(), cclock);
	ts->tv_sec = mts.tv_sec;
	ts->tv_nsec = mts.tv_nsec;
}

#elif defined(CLOCK_MONOTONIC) && defined(CLOCK_REALTIME)

static clockid_t clk_id;

int nanotime_setup(void)
{
	struct timespec ts;

	if (!clock_gettime(CLOCK_MONOTONIC, &ts)) {
		clk_id = CLOCK_MONOTONIC;
		return 0;
	}
	if (!clock_gettime(CLOCK_REALTIME, &ts)) {
		clk_id = CLOCK_REALTIME;
		return 0;
	}
	return -1;
}

void get_nanotime(struct timespec *ts)
{
	if (clock_gettime(clk_id, ts)) {
		pdie("clock_gettime failed");
	}
}

#else

int nanotime_setup(void)
{
	journal("Using gettimeofday() for nanotime :(");
	return 0;
}

void get_nanotime(struct timespec *ts)
{
	struct timeval tv;

	if (gettimeofday(&tv, NULL)) {
		pdie("gettimeofday() failed");
	}
	ts->tv_sec = tv.tv_sec;
	ts->tv_nsec = tv.tv_usec * 1000;
}

#endif
