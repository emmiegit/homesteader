/*
 * arguments.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "arguments.h"
#include "core.h"

struct options opt;

static void print_usage(FILE *fh, const char *program_name)
{
	fprintf(fh,
		"Usage:\n"
		" %s [arguments]\n"
		" %s --help | --version\n",
	       program_name, program_name);
	if (fh == stderr) {
		exit(EXIT_FAILURE);
	}
}

static void print_help(const char *program_name)
{
	print_usage(stdout, program_name);
	printf("\n"
	       " -j, --journal [path]       Use the following as the journal file.\n"
	       " -R, --resources [dir]      Set the resource directory. (Default is %s).\n"
	       "     --help                 Print this help message.\n"
	       "     --version              Print the version and quit.\n",
	       DEFAULT_RESOURCE_DIR);
}

void parse_args(int argc, const char *argv[])
{
	int i;

	opt.journal_path = NULL;
	opt.resource_dir = DEFAULT_RESOURCE_DIR;

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-j") || !strcmp(argv[i], "--journal")) {
			if (i > argc - 1) {
				fprintf(stderr, "'%s' requires an argument.\n", argv[i]);
				print_usage(stderr, argv[0]);
				exit(EXIT_FAILURE);
			}
			opt.journal_path = argv[++i];
		} else if (!strcmp(argv[i], "-R") || !strcmp(argv[i], "--resources")) {
			if (i > argc - 1) {
				fprintf(stderr, "'%s' requires an argument.\n", argv[i]);
				print_usage(stderr, argv[0]);
				exit(EXIT_FAILURE);
			}
			opt.resource_dir = argv[++i];
		} else if (!strcmp(argv[i], "--help")) {
			print_help(argv[0]);
			exit(EXIT_SUCCESS);
		} else if (!strcmp(argv[i], "--version")) {
			print_version();
			exit(EXIT_SUCCESS);
		} else {
			fprintf(stderr, "Unknown argument: '%s'.\n\n", argv[i]);
			print_usage(stderr, argv[0]);
			exit(EXIT_FAILURE);
		}
	}
}
