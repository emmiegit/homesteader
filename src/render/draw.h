/*
 * draw.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RENDER_DRAW_H
#define __RENDER_DRAW_H

#include "level.h"
#include "map.h"
#include "screen.h"

void draw_setup(void);
void draw_cleanup(void);

void draw_title(WINDOW *win, const char *title, int row);
void draw_map(WINDOW *win, const struct map *map);
void draw(const struct screen *scr);

#endif /* __RENDER_DRAW_H */

/* vim: set ft=c: */
