/*
 * screen.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RENDER_SCREEN_H
#define __RENDER_SCREEN_H

#include <curses.h>

struct screen {
	WINDOW *main_win;
	WINDOW *line_win;
	WINDOW *title_win;
	WINDOW *status_win;
	int x, y;
};

void curses_on(void);
void curses_off(void);
void resize(void);
void full_redraw(void);
void redraw(void);
void set_title(const char *title);

int curses_active(void);
void screen_size(int *x, int *y);

void screen_setup(void);
void screen_cleanup(void);
void screen_event_hint(void);
void *screen_mainloop(void *arg);

#endif /* __RENDER_SCREEN_H */

/* vim: set ft=c: */
