/*
 * draw.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curses.h>

#include <string.h>

#include "render/menus.h"

#include "draw.h"
#include "game.h"
#include "state.h"

/* Utilities */

static void path_draw(WINDOW *win)
{
	const chtype attrs = COLOR_PAIR(CLR_VISIT);

	CURSES_CALL(wattron(win, attrs));
	CURSES_CALL(waddch(win, ' '));
	CURSES_CALL(wattroff(win, attrs));
}

static void entity_draw(WINDOW *win, const struct entity *ent)
{
	const chtype attrs = COLOR_PAIR(ent->sprite.color) | ent->sprite.attr;

	CURSES_CALL(wmove(win, ent->pos.y, ent->pos.x));
	CURSES_CALL(wattron(win, attrs));
	CURSES_CALL(waddstr(win, ent->sprite.str));
	CURSES_CALL(wattroff(win, attrs));
}

static void status_draw(WINDOW *win)
{
	CURSES_CALL(werase(win));
	CURSES_CALL(wmove(win, 0, 1));
	CURSES_CALL(wprintw(win, "NORMAL"));
}

static void draw_level(const struct screen *scr)
{
	CURSES_CALL(curs_set(2));
	path_draw(scr->main_win);
	status_draw(scr->status_win);
	CURSES_CALL(wmove(scr->main_win, game.player.y, game.player.x));
}

/* Externals */

void draw_setup(void)
{
	/* nothing to do */

	UNUSED(entity_draw /* FIXME */);
}

void draw_cleanup(void)
{
	/* nothing to do */
}

void draw_title(WINDOW *win, const char *title, int row)
{
	int win_x, win_y;
	int offset;

	UNUSED(win_y);

	getmaxyx(win, win_y, win_x);
	offset = (win_x - strlen(title)) / 2;
	CURSES_CALL(mvwaddstr(win, row, offset, title));
}

void draw_map(WINDOW *win, const struct map *map)
{
	UNUSED(win);
	UNUSED(map);

	/* TODO */
}

void draw(const struct screen *scr)
{
	const struct state *st;

	st = state_r_get();
	switch (st->type) {
	case STATE_TYPE_MENU:
		menus_draw(scr, &st->u.menu);
		break;
	case STATE_TYPE_LEVEL:
		draw_level(scr);
	}
	state_release(st);
}
