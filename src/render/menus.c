/*
 * menu.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curses.h>

#include <string.h>

#include "render/menus.h"

#include "colors.h"
#include "core.h"
#include "draw.h"
#include "main.h"
#include "state.h"

#define BOX_HEIGHT	3
#define INVERSE_PHI	0.6180339887498948

static const char *menu_strings[] = {
	"NEW",
	"LOAD",
	"OPTIONS",
	"QUIT"
};

/* Utilities */

static void add_box(WINDOW *win, const char *str, int x, int y, int width, int height, int em)
{
	const int attrs = COLOR_PAIR(CLR_SELECT) | A_BOLD;
	int i;

	if (em) {
		CURSES_CALL(wattron(win, attrs));
	}

	CURSES_CALL(mvwaddch(win, y,          x,         ACS_ULCORNER));
	CURSES_CALL(mvwaddch(win, y + height, x,         ACS_LLCORNER));
	CURSES_CALL(mvwaddch(win, y,          x + width, ACS_URCORNER));
	CURSES_CALL(mvwaddch(win, y + height, x + width, ACS_LRCORNER));

	for (i = 1; i < width; i++) {
		CURSES_CALL(mvwaddch(win, y,          x + i, ACS_HLINE));
		CURSES_CALL(mvwaddch(win, y + height, x + i, ACS_HLINE));
	}
	for (i = 1; i < height; i++) {
		CURSES_CALL(mvwaddch(win, y + i, x,         ACS_VLINE));
		CURSES_CALL(mvwaddch(win, y + i, x + width, ACS_VLINE));
	}
	draw_title(win, str, y + (height / 2));

	if (em) {
		CURSES_CALL(wattroff(win, attrs));
	}

}

static void add_boxes(WINDOW *win, const char **strs, unsigned int select, unsigned int count)
{
	unsigned int i;
	int win_x, win_y;
	int box_x, box_y;
	int spc_x, spc_y;

	ASSERT(count > 0);

	/*
	 * Boxes are 3 high, one border, one text, one border.
	 * Calculate the width of these boxes as a proportion
	 * of the full screen and the golden ratio.
	 *
	 * Then given the number of units, compute the amount
	 * of space in between each box.
	 */

	getmaxyx(win, win_y, win_x);
	box_x = (int)(win_x * INVERSE_PHI);
	box_y = BOX_HEIGHT - 1;
	spc_x = (win_x - box_x) / 2;
	spc_y = (win_y - (box_y * count)) / (count + 1);

	CURSES_CALL(werase(win));
	for (i = 0; i < count; i++) {
		int x, y, em;

		x = spc_x;
		y = (spc_y * (i + 1)) + (box_y * i);
		em = (select == i);
		add_box(win, strs[i], x, y, box_x, box_y, em);
	}
}

static void main_menu_draw(const struct screen *scr, const struct menu *menu)
{
	CURSES_CALL(werase(scr->line_win));
	CURSES_CALL(werase(scr->title_win));
	draw_title(scr->title_win, PROGRAM_NAME, 0);

	CURSES_CALL(werase(scr->status_win));
	switch (menu->u.main.sel) {
	case MENU_ITEM_NEW:
		waddstr(scr->status_win, "Start a new game");
		break;
	case MENU_ITEM_LOAD:
		waddstr(scr->status_win, "Load an existing save file");
		break;
	case MENU_ITEM_OPTIONS:
		waddstr(scr->status_win, "Change game settings");
		break;
	case MENU_ITEM_QUIT:
		waddstr(scr->status_win, "Exit the application");
		break;
	default:
		die("Invalid main menu item value: %u\n", menu->u.main.sel);
	}

	add_boxes(scr->main_win, menu_strings, menu->u.main.sel, 4);
}

static void new_menu_draw(const struct screen *scr, const struct menu *menu)
{
	UNUSED(scr);
	UNUSED(menu);

	/* TODO */

	cleanup(2);
}

static void load_menu_draw(const struct screen *scr, const struct menu *menu)
{
	UNUSED(scr);
	UNUSED(menu);

	/* TODO */

	cleanup(2);
}

static void options_menu_draw(const struct screen *scr, const struct menu *menu)
{
	UNUSED(scr);
	UNUSED(menu);

	/* TODO */

	cleanup(2);
}

/* Externals */

void menus_draw(const struct screen *scr, const struct menu *menu)
{
	curs_set(0);
	switch (menu->stage) {
	case MENU_STAGE_MAIN:
		main_menu_draw(scr, menu);
		break;
	case MENU_STAGE_NEW:
		new_menu_draw(scr, menu);
		break;
	case MENU_STAGE_LOAD:
		load_menu_draw(scr, menu);
		break;
	case MENU_STAGE_OPTIONS:
		options_menu_draw(scr, menu);
		break;
	default:
		die("Invalid menu stage value: %u\n", menu->stage);
	}
	CURSES_CALL(wmove(scr->main_win, 0, 0));
}
