/*
 * screen.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curses.h>
#include <pthread.h>

#include <math.h>
#include <stdlib.h>

#include "colors.h"
#include "core.h"
#include "draw.h"
#include "game.h"
#include "screen.h"
#include "signal_handler.h"

#define QUEUE_CAPACITY		32

static struct {
	size_t head;
	size_t size;
	enum request_type {
		DO_NOTHING,
		RESIZE,
		REDRAW,
		FULL_REDRAW
	} entries[QUEUE_CAPACITY];
} queue;

static pthread_mutex_t queue_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t queue_cond = PTHREAD_COND_INITIALIZER;
static int active;

static struct screen scr;

/* Pthread helpers */

static void lock(void)
{
	if (pthread_mutex_lock(&queue_lock)) {
		TRACE();
	}
}

static void unlock(void)
{
	if (pthread_mutex_unlock(&queue_lock)) {
		TRACE();
	}
}

static void cond_wait(void)
{
	if (pthread_cond_wait(&queue_cond, &queue_lock)) {
		TRACE();
	}
}

static void cond_signal(void)
{
	if (pthread_cond_signal(&queue_cond)) {
		TRACE();
	}
}

/* Job queue */

static void queue_push(enum request_type item)
{
	lock();
	if (queue.size == QUEUE_CAPACITY) {
		journal("Render queue is full! Dumping items.");
		queue.size = 0;
	}
	queue.entries[(queue.head + queue.size) % QUEUE_CAPACITY] = item;
	queue.size++;
	cond_signal();
	unlock();
}

static enum request_type queue_pop(void)
{
	enum request_type item;

	lock();
	while (queue.size == 0) {
		cond_wait();
	}
	item = queue.entries[queue.head];
	queue.head = (queue.head + 1) % QUEUE_CAPACITY;
	queue.size--;
	unlock();
	return item;
}

/* Queued operations */

static void scr_update(void)
{
	CURSES_CALL(wrefresh(scr.title_win));
	CURSES_CALL(wrefresh(scr.status_win));
	CURSES_CALL(wrefresh(scr.line_win));
	CURSES_CALL(wrefresh(scr.main_win));
}

static void do_redraw(void)
{
	if (!active) {
		journal("Can't do a redraw without curses mode.");
		return;
	}

	journal("Redrawing screen.");
	draw(&scr);
	scr_update();
}

static void do_full_redraw(void)
{
	if (!active) {
		journal("Can't do a full redraw without curses mode.");
		return;
	}

	journal("Fully redrawing screen.");
	CURSES_CALL(clear());
	draw(&scr);
	scr_update();
}

static void do_resize(void)
{
	journal("Resizing screen.");
	curses_off();
	curses_on();
	do_full_redraw();
}

static void do_action(enum request_type action)
{
	switch (action) {
	case DO_NOTHING:
		break;
	case RESIZE:
		do_resize();
		break;
	case FULL_REDRAW:
		do_full_redraw();
		break;
	case REDRAW:
		do_redraw();
	}
}

/* Curses mode */

static void setup_windows(void)
{
	const int lineno_len = (int)ceil(log10(LINES - 1));

	scr.title_win = newwin(1, COLS, 0, 0);
	if (!scr.title_win) {
		pdie("Unable to create title window");
	}
	scr.status_win = newwin(1, COLS, LINES - 1, 0);
	if (!scr.status_win) {
		pdie("Unable to create status window");
	}
	scr.line_win = newwin(LINES - 2, lineno_len, 1, 0);
	if (!scr.line_win) {
		pdie("Unable to create line number window");
	}
	scr.main_win = newwin(LINES - 2, COLS - lineno_len, 1, lineno_len);
	if (!scr.main_win) {
		pdie("Unable to create main window");
	}
	CURSES_CALL(leaveok(scr.main_win, FALSE));
	CURSES_CALL(keypad(scr.main_win, TRUE));
	CURSES_CALL(intrflush(scr.main_win, FALSE));
	CURSES_CALL(idlok(scr.main_win, TRUE));
	idcok(scr.main_win, TRUE);

	getmaxyx(scr.main_win, scr.y, scr.x);
}

static void destroy_windows(void)
{
	CURSES_CALL(delwin(scr.title_win));
	CURSES_CALL(delwin(scr.status_win));
	CURSES_CALL(delwin(scr.line_win));
	CURSES_CALL(delwin(scr.main_win));
}

void curses_on(void)
{
	const char *term;

	if (active) {
		journal("Curses mode already active.");
		return;
	}

	lock();
	initscr();
	active = 1;
	journal("Starting curses mode, size: %d x %d.", COLS, LINES);
	term = getenv("TERM");
	if (term) {
		journal("Terminal name: %s.", term);
	} else {
		journal("No $TERM variable set in environment.");
	}
	setup_windows();
	colors_init();
	CURSES_CALL(cbreak());
	CURSES_CALL(nonl());
	CURSES_CALL(noecho());
	unlock();
}

void curses_off(void)
{
	if (!active) {
		journal("Curses mode already inactive.");
		return;
	}

	lock();
	active = 0;
	journal("Ending curses mode.");
	destroy_windows();
	CURSES_CALL(endwin());
	unlock();
}

int curses_active(void)
{
	return active;
}

/* Externals */

void screen_setup(void)
{
	/* nothing to do */
}

void screen_cleanup(void)
{
	if (pthread_mutex_destroy(&queue_lock)) {
		TRACE();
	}
	if (pthread_cond_destroy(&queue_cond)) {
		TRACE();
	}
}

void *screen_mainloop(void *arg)
{
	UNUSED(arg);

	journal("Beginning render main loop.");
	while (game.state != GAME_STOPPED) {
		do_action(queue_pop());
	}
	return NULL;
}

void screen_event_hint(void)
{
	queue_push(DO_NOTHING);
}

void resize(void)
{
	queue_push(RESIZE);
}

void redraw(void)
{
	queue_push(REDRAW);
}

void full_redraw(void)
{
	queue_push(FULL_REDRAW);
}

void screen_size(int *x, int *y)
{
	getmaxyx(scr.main_win, *y, *x);
	(*y)--;
	(*x)--;
}

#if defined(_WIN32)
# include <windows.h>
#endif /* _WIN32 */

void set_title(const char *title)
{
	const char *term;

	term = getenv("TERM");
	if (term && strstr(term, "xterm")) {
		journal("Setting window title to \"%s\".", title);
		printf("\x1b]0;%s\a", title);
	} else {
#if defined(_WIN32)
		if (!SetConsoleTitle(title)) {
			journal("Unable to set CMD title. (%lu).", (unsigned long)GetLastError());
		}
#else
		journal("Terminal is not an xterm, not using control sequence.");
#endif /* _WIN32 */
	}
}
