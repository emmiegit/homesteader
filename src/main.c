/*
 * main.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include "render/screen.h"

#include "arguments.h"
#include "core.h"
#include "engine.h"
#include "game.h"
#include "input.h"
#include "main.h"
#include "nanotime.h"
#include "random.h"
#include "resources.h"
#include "serial.h"
#include "signal_handler.h"
#include "sound.h"
#include "state.h"

static void do_setup(int argc, const char *argv[])
{
	core_setup();
	parse_args(argc, argv);
	if (journal_setup()) {
		fprintf(stderr, "Unable to open journal \"%s\": %s.",
			opt.journal_path, strerror(errno));
		cleanup(EXIT_FAILURE);
	}
	signals_setup();
	random_setup();
	if (nanotime_setup()) {
		fprintf(stderr, "Unable to find a suitable clock for nanosecond precision.\n");
		cleanup(EXIT_FAILURE);
	}
	if (resources_setup()) {
		fprintf(stderr, "Unable to open resource directory \"%s\": %s.",
			opt.resource_dir, strerror(errno));
		cleanup(EXIT_FAILURE);
	}
	state_setup();
	sound_setup();
	screen_setup();
	serial_setup();
	game_setup();
	set_title(PROGRAM_NAME);
}

int main(int argc, const char *argv[])
{
	do_setup(argc, argv);
	engine_start();
	primary_mainloop();
}

void cleanup(int ret)
{
	journal("Exiting with return code %d.", ret);
	engine_stop();
	game_cleanup();
	sound_cleanup();
	screen_cleanup();
	resources_cleanup();
	journal_close();
	exit(ret);
}
