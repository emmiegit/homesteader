/*
 * core.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "render/screen.h"

#include "arguments.h"
#include "core.h"
#include "main.h"

/* Endian conversion */

static int big_endian;

void core_setup(void)
{
	unsigned int u;

	STATIC_ASSERT(CHAR_BIT == 8);

	u = 0x23u;
	u = *((unsigned char *)(&u));
	big_endian = (u != 0x23u);
}

uint16_t proper16(uint16_t x)
{
	union {
		uint16_t x;
		char c[2];
	} a, b;

	if (big_endian) {
		a.x = x;
		b.c[0] = a.c[1];
		b.c[1] = a.c[0];
		return b.x;
	}
	return x;
}

uint32_t proper32(uint32_t x)
{
	union {
		uint32_t x;
		char c[4];
	} a, b;

	if (big_endian) {
		a.x = x;
		b.c[0] = a.c[3];
		b.c[1] = a.c[2];
		b.c[2] = a.c[1];
		b.c[3] = a.c[0];
		return b.x;
	}
	return x;
}

uint64_t proper64(uint64_t x)
{
	union {
		uint64_t x;
		char c[8];
	} a, b;

	if (big_endian) {
		a.x = x;
		b.c[0] = a.c[7];
		b.c[1] = a.c[6];
		b.c[2] = a.c[5];
		b.c[3] = a.c[4];
		b.c[4] = a.c[3];
		b.c[5] = a.c[2];
		b.c[6] = a.c[1];
		b.c[7] = a.c[0];
		return b.x;
	}
	return x;
}

/* Asserts */

void die(const char *format, ...)
{
	va_list args;

	TRACE();
	curses_off();
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);
	fputc('\n', stderr);
	cleanup(-1);
}

void pdie(const char *message)
{
	TRACE();
	curses_off();
	if (errno) {
		journal("Fatal: %s: %s.", message, strerror(errno));
		perror(message);
	} else {
		journal("Fatal: %s.", message);
		fprintf(stderr, "%s.\n", message);
	}
	cleanup(-1);
}

/* Other utilities */

void print_version(void)
{
	printf("%s v%d.%d.%d [%s]\n"
	       "(%s %s) on %s.\n"
	       "Built %s, %s.\n"
	       "\n"
	       "%s is free software: you can redistribute it and/or modify\n"
	       "it under the terms of the GNU General Public License as published by\n"
	       "the Free Software Foundation, version 2 or later.\n",
	       PROGRAM_NAME,
	       PROGRAM_VERSION_MAJOR,
	       PROGRAM_VERSION_MINOR,
	       PROGRAM_VERSION_PATCH,
	       GIT_HASH,
	       COMPILER_NAME,
	       COMPILER_VERSION,
	       PLATFORM_NAME,
	       __DATE__,
	       __TIME__,
	       PROGRAM_NAME);
}
