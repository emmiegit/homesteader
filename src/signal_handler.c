/*
 * signal_handler.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <signal.h>

#include "main.h"
#include "signal_handler.h"

static void handle_signal(int signum)
{
	journal("Caught signal %d.", signum);
	switch (signum) {
	case SIGTERM:
		cleanup(0);
		break;
	case SIGINT:
		cleanup(-1);
		break;
	case SIGSEGV:
		die("Error: segmentation fault");
		break;
	case SIGILL:
		die("Error: illegal instruction");
		break;
	case SIGFPE:
		die("Error: arithmetic exception");
		break;
#ifdef SIGHUP
	case SIGHUP:
		cleanup(0);
#endif /* SIGHUP */
#ifdef SIGPIPE
	case SIGPIPE:
		die("Error: broken pipe");
		break;
#endif /* SIGPIPE */
	default:
		die("No such signal handler");
	}
}

void signals_setup(void)
{
	journal("Setting up signal handlers.");
	signal(SIGTERM,  handle_signal);
	signal(SIGINT,   handle_signal);
	signal(SIGSEGV,  handle_signal);
	signal(SIGILL,   handle_signal);
	signal(SIGFPE,   handle_signal);
#ifdef SIGHUP
	signal(SIGHUP,   handle_signal);
#endif /* SIGHUP */
#ifdef SIGPIPE
	signal(SIGPIPE,  handle_signal);
#endif /* SIGPIPE */
}
