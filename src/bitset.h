/*
 * bitset.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BITSET_H
#define __BITSET_H

#include <stddef.h>
#include <stdint.h>

struct bitset {
	uintmax_t *blocks;
	size_t block_count;
	unsigned int size;
};

int bitset_create(struct bitset *bs, unsigned int size, int inital);
void bitset_destroy(struct bitset *bs);

int bitset_get(const struct bitset *bs, unsigned int index);
void bitset_set(struct bitset *bs, unsigned int index);
void bitset_clear(struct bitset *bs, unsigned int index);
void bitset_toggle(struct bitset *bs, unsigned int index);
void bitset_assign(struct bitset *bs, unsigned int index, int value);

#endif /* __BITSET_H */

/* vim: set ft=c: */
