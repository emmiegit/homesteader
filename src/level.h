/*
 * level.h
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __LEVEL_H
#define __LEVEL_H

#include <stddef.h>
#include <time.h>

#include "position.h"

struct map;

#define MIN_LEVEL_WIDTH		16
#define MIN_LEVEL_HEIGHT	12

enum level_pos {
	LEVEL_START_TL,
	LEVEL_START_TC,
	LEVEL_START_TR,
	LEVEL_START_ML,
	LEVEL_START_MC,
	LEVEL_START_MR,
	LEVEL_START_BL,
	LEVEL_START_BC,
	LEVEL_START_BR,
	LEVEL_START_FIRST,
	LEVEL_START_LAST
};

struct level {
	/* Data */
	struct map *map;

	enum level_pos start;
	struct position end;

	size_t max_key_presses;
	time_t max_time;

	/* Runtime */
	enum mode {
		MODE_NORMAL,
		MODE_INSERT,
		MODE_VISUAL_LINE,
		MODE_VISUAL_BLOCK
	} mode;
	struct {
		char hist[256];
		size_t count;
	} keys;
};

void level_init(struct level *lvl, struct map *map);
int level_hist_append(struct level *lvl, char ch);

#define level_hist_clear(lvl)				\
	do {						\
		(lvl)->keys.count = 0;			\
	} while (0)


#endif /* __LEVEL_H */

/* vim: set ft=c: */
