/*
 * input.c
 *
 * homesteader - A curses farming game.
 * Copyright (c) 2016 Ammon Smith
 *
 * homesteader is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * homesteader is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with homesteader.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <curses.h>
#include <sched.h>

#include <ctype.h>

#include "render/draw.h"
#include "render/screen.h"

#include "core.h"
#include "game.h"
#include "input.h"
#include "main.h"
#include "sound.h"
#include "state.h"

#define ESCAPE		27

struct action {
	unsigned int mod;
	enum {
		YANK,
		DELETE,
		CHANGE
	} command;
	enum {
		MOVE_UP,
		MOVE_DOWN,
		MOVE_LEFT,
		MOVE_RIGHT,
		LINE_BEGIN,
		LINE_END,
		FILE_BEGIN,
		FILE_MIDDLE,
		FILE_END
	} movement;
	enum {
		NORMAL,
		VOLUME_UP,
		VOLUME_DOWN,
		SELECT,
		RESIZE,
		REDRAW,
		QUIT
	} special;
};

static int get_char(void)
{
	int ch;

	ch = getch();
	if (ch == ERR) {
		journal("Couldn't read character from user.");
		cleanup(1);
	}
	return ch;
}

static int g_command(struct action *act)
{
	switch (get_char()) {
	case 'g':
		act->movement = FILE_BEGIN;
		break;
	default:
		return 0;
	}
	return 1;
}

static int parse_action(struct action *act, int ch)
{
	/* Special keys */
	switch (ch) {
	case KEY_RESIZE:
		act->special = RESIZE;
		return 1;
	case KEY_REFRESH:
		act->special = REDRAW;
		return 1;
	case '\r':
	case '\n':
		act->special = SELECT;
		return 1;
	case KEY_CLOSE:
	case KEY_EXIT:
	case 'Q':
		act->special = QUIT;
		return 1;
	default:
		act->special = NORMAL;
	}

	/* Modifier */
	act->mod = 0;
	if (ch != '0') {
		while (isdigit(ch)) {
			act->mod *= 10;
			act->mod += (ch - '0');
			ch = get_char();
			if (ch == ESCAPE) {
				return 0;
			}
		}
	}
	if (act->mod == 0) {
		act->mod = 1;
	}

	/* Action */
	switch (ch) {
	case 'y':
		act->command = YANK;
		break;
	case 'd':
	case 'x':
		act->command = DELETE;
		break;
	case 'c':
	case 's':
		act->command = CHANGE;
		break;
	}

	/* Movement */
	switch (ch) {
	case KEY_LEFT:
	case 'h':
		act->movement = MOVE_LEFT;
		break;
	case KEY_RIGHT:
	case 'l':
		act->movement = MOVE_RIGHT;
		break;
	case KEY_UP:
	case 'k':
		act->movement = MOVE_UP;
		break;
	case KEY_DOWN:
	case 'j':
		act->movement = MOVE_DOWN;
		break;
	case '0':
	case '^':
		act->movement = LINE_BEGIN;
		break;
	case '$':
		act->movement = LINE_END;
		break;
	case 'g':
		return g_command(act);
	case 'H':
		act->movement = FILE_BEGIN;
		break;
	case 'M':
		act->movement = FILE_MIDDLE;
		break;
	case 'G':
	case 'L':
		act->movement = FILE_END;
		break;
	default:
		return 0;
	}
	return 1;
}

static void set_new_position(int new_x, int new_y)
{
	struct state *st;

	/* Set the new position */
	st = state_w_get();
	switch (st->type) {
	case STATE_TYPE_LEVEL:
		game.player.x = new_x;
		game.player.y = new_y;
		break;
	case STATE_TYPE_MENU:
		UNUSED(new_x);
		st->u.menu.u.main.sel = new_y;
		break;
	}
	state_release(st);
}

static void run_action(const struct action *act)
{
	const struct state *st;
	int max_x, max_y;
	int old_x, old_y;
	int new_x, new_y;

	/* Special cases */
	st = state_r_get();
	switch (act->special) {
	case NORMAL:
		break;
	case VOLUME_UP:
		sound_volume(+1);
		break;
	case VOLUME_DOWN:
		sound_volume(-1);
		break;
	case SELECT: {
		const enum main_menu_item type = st->type;
		state_release(st);
		switch (type) {
		case STATE_TYPE_MENU:
			journal("Select called on a menu.");
			menu_select();
			break;
		default:
			journal("Select called when not in a menu.");
			break;
		}
		return;
	     }
	case RESIZE:
		resize();
		state_release(st);
		return;
	case REDRAW:
		full_redraw();
		state_release(st);
		return;
	case QUIT:
		cleanup(0);
	}

	/* Determine where we can move */
	switch (st->type) {
	case STATE_TYPE_LEVEL:
		old_x = new_x = game.player.x;
		old_y = new_y = game.player.y;
		screen_size(&max_x, &max_y);
		break;
	case STATE_TYPE_MENU:
		old_x = new_x = 0;
		old_y = new_y = st->u.menu.u.main.sel;
		max_x = 0;
		max_y = NUM_MENU_ITEMS - 1;
		break;
	default:
		die("Invalid value for draw_state type: %u\n", st->u.menu.u.main.sel);
	}
	state_release(st);

	/* Calculate the new position */
	switch (act->movement) {
	case MOVE_LEFT:
		new_x = max(old_x - act->mod, 0);
		break;
	case MOVE_RIGHT:
		new_x = min(old_x + act->mod, max_x - 1);
		break;
	case MOVE_UP:
		new_y = max(old_y - act->mod, 0);
		break;
	case MOVE_DOWN:
		new_y = min(old_y + act->mod, max_y);
		break;
	case LINE_BEGIN:
		new_x = 0;
		break;
	case LINE_END:
		new_x = max_x - 1;
		break;
	case FILE_BEGIN:
		new_y = 0;
		break;
	case FILE_MIDDLE:
		new_y = max_y / 2;
		break;
	case FILE_END:
		new_y = max_y;
		break;
	}

	set_new_position(new_x, new_y);

	/* TODO */
	UNUSED(act->command);
}

void primary_mainloop(void)
{
	/* Wait for curses to initialize */
	while (!curses_active()) {
		sched_yield();
		hint_pause();
	}

	journal("Beginning primary main loop.");
	for (;;) {
		struct action act;
		const int ch = get_char();
		journal("Got event from user.");
		if (parse_action(&act, ch)) {
			run_action(&act);
		}
		redraw();
	}
}
