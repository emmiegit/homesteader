/* Definitions */
#define NULL					((void *)0)

/* core.h */
#define UNUSED(x)				((void)(x))
#define FINAL_FREE(x)				\
	do {					\
		__coverity_free__(x);		\
	} while (0)

void hint_pause(void)
{
	__coverity_sleep__();
}

/* core.c */
void die(const char *format, ...)
{
	UNUSED(format);
	__coverity_panic__();
}

void pdie(const char *message)
{
	UNUSED(message);
	__coverity_panic__();
}

/* main.c */
void cleanup(int ret)
{
	UNUSED(ret);
	__coverity_panic__();
}

/* journal.c */
void journal(const char *format, ...)
{
	UNUSED(format);
	__coverity_format_string_sink__(format);
}


