REM Update cygwin setup
bitsadmin.exe /transfer "JobName" "http://cygwin.com/setup-x86_64.exe" C:\cygwin64\setup-x86.exe

C:\cygwin64\setup-x86_64.exe -qnNdO ^
    -R "C:/cygwin64" -s "http://cygwin.mirror.constant.com" -l "C:/cygwin64/var/cache/setup" ^
    -P mingw64-x86_64-gcc-core ^
    -P mingw64-x86_64-pthreads ^
    -P mingw64-x86_64-ncurses ^
    -P mingw64-x86_64-libao

REM replace -pthreads with -winpthreads?
