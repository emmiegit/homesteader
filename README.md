## Homesteader
[![Build Status (\*nix)](https://travis-ci.org/ammongit/homesteader.svg?branch=master)](https://travis-ci.org/ammongit/homesteader)
[![Build Status (Windows)](https://ci.appveyor.com/api/projects/status/github/ammongit/homesteader?branch=master&svg=true)](https://ci.appveyor.com/project/ammongit/homesteader)
[![Coverity Status](https://scan.coverity.com/projects/10216/badge.svg)](https://scan.coverity.com/projects/ammongit-homesteader)

Homesteader is a curses game.

### Requirements for \*nix (including OS X)
* ANSI C compiler
* POSIX 1995 compatibility
* POSIX shell
* GNU Make
* ncurses 5 or later
* libao

If you meet these requirements but it doesn't compile on your system, please open an issue.

### Requirements for Windows
Cygwin is required to compile for Windows. Once installed, the following Cygwin packages are required:
(Obviously if you're on i686 you should use those packages instead of x86\_64).
* make
* mingw64-x86\_64-gcc-core
* mingw64-x86\_64-winpthread
* mingw64-x86\_64-ncurses
* mingw64-x86\_64-libao

[Instructions for building on Windows](https://github.com/ammongit/homesteader/issues/1#issuecomment-271697527)

### Compilation
In the top level directory of the repository, run:
```
$ make release
```
See `make targets` for a list of build targets.

### Running
Run the `homesteader` binary in `src`.
Use `--help` or see the man page for more details.

### License
GPLv2+

